﻿#include <iostream>
#include <string>

class Player
{
public:
    std::string Name;
    int Score;

    Player(std::string _name = "none", int _score = 0) : Name(_name), Score(_score)
    {
    }
};

void bubblesort(Player* ArrayPlayers, int CountPlayers)
{
    bool b = true;
    while (b)
    {
        b = false;
        for (int i = 0; i < CountPlayers - 1; i++)
        {
            if (ArrayPlayers[i].Score < ArrayPlayers[i + 1].Score)
            {
                std::swap(ArrayPlayers[i], ArrayPlayers[i + 1]);
                b = true;
            }
        }
    }
}

void SetPlayerInfo(Player* ArrayPlayers, int CountPlayers)
{
    for (int i = 0; i < CountPlayers; ++i)
    {
        std::cout << "\nEnter name: ";
        std::cin >> ArrayPlayers[i].Name;
        std::cout << "Enter Score: ";
        std::cin >> ArrayPlayers[i].Score;
    }
}

int main()
{
    int CountPlayers;

    std::cout << "Enter count players: ";
    std::cin >> CountPlayers;
    auto ArrayPlayers = new Player[CountPlayers];

    SetPlayerInfo(ArrayPlayers, CountPlayers);
    bubblesort(ArrayPlayers, CountPlayers);

    std::cout << "\n|Leaderboard|\n";
    for (int i = 0; i < CountPlayers; ++i)
    {
        std::cout << std::endl << ArrayPlayers[i].Name << " " << ArrayPlayers[i].Score;
    }
    delete[] ArrayPlayers;
}